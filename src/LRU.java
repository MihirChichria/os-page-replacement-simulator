
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class LRU {
    private int hits;
    private float hitRatio;
    int findLRU(int time[], int n){
	int i, minimum = time[0], index = 0;
 
	for(i = 1; i < n; ++i){
            if(time[i] < minimum){
                minimum = time[i];
                index = i;
            }
	}
	
	return index;
    }
    void generatePages(int frameSize, ArrayList<Integer> pagesList, JPanel displayPagePanel){
        int  frames[] = new int[frameSize],  counter = 0, time[] = new int[10], flag1, flag2, i, j, index, hit = 0;
	
        for(i=0; i<frameSize; i++){
            frames[i] = -1;
        }
    
        for(i=0; i<pagesList.size(); i++){
            flag1 = flag2 = 0;
            for(j=0; j<frameSize; j++){
                if(frames[j] == pagesList.get(i)){
                    // Element is present in frame[], just setting the counter++ 
                    time[j] = ++counter;
                    flag1 = flag2 = 1;
                    hit++;
                    generateFrame(frames, displayPagePanel, pagesList.get(i) + " ( Hit ) ", pagesList);
                    break;
                }
            }

            // If the element is not present in frame and frame has empty places!
            if(flag1 == 0){
                for(j=0; j<frameSize; j++){
                    if(frames[j] == -1){
                        frames[j] = pagesList.get(i);
                        time[j] = ++counter;
                        flag2 = 1;
                        generateFrame(frames, displayPagePanel, pagesList.get(i) + " ( Miss ) ", pagesList);
                        break;
                    }
                }	
            }
            // If there is no empty place in frame[]
            if(flag2 == 0){
                // findLRU will return index which is least counter.
                index = findLRU(time, frameSize);
                frames[index] = pagesList.get(i);
                time[index] = ++counter;
                generateFrame(frames, displayPagePanel, pagesList.get(i) + " ( Miss ) ", pagesList);
            }

//            System.out.println("");
//            System.out.println("For: " + pagesList.get(i));
//            for(j=0; j<frameSize; j++){
//                System.out.print("\t" + frames[j]);
//            }
        }   
        this.hits = hit;
        this.hitRatio = (float)(((float)this.hits/(float)pagesList.size())*100.0f);
    }

    private void generateFrame(int[] frame, JPanel displayPagePanel, String status, ArrayList<Integer> pagesList) {
        displayPagePanel.setLayout(new GridLayout(pagesList.size()/5, 5));
        
        PageFrame pf = new PageFrame(frame, status);
        displayPagePanel.add(pf);
        
        pf.validate();
        pf.updateUI();
    }
    int getHits(){
        return this.hits;
    }
    float getHitRatio(){
        return this.hitRatio;
    }
}
